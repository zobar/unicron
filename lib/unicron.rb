##
# Unicron provides a simple mechanism for scheduling one-off and repeating tasks
# to execute at specific times in the background of a long-running Ruby process.
#
# Tasks are represented as Job instances added to a JobQueue. A Schedule
# manages a single background thread which pops items off the JobQueue and runs
# them one at a time. Unicron manages a default Schedule for simplicity, but it
# is safe to create more - each will run its own thread with its own JobQueue.
# Since a Schedule only runs one Job at a time, a Job that is blocked or
# processing could potentially delay other Jobs on the same Schedule. For that
# reason, you may want to use a separate Schedule for long-running tasks.
#
module Unicron
  autoload :Job,      'unicron/job'
  autoload :JobQueue, 'unicron/job_queue'
  autoload :Schedule, 'unicron/schedule'

  ## Provide access to the default Unicron Schedule.
  def self.default_schedule
    @@default_schedule ||= Schedule.new
  end

  ##
  # Create a new Job and add it to the default Schedule. See Job::new for
  # available options.
  #
  def self.schedule options={}, &block
    default_schedule.schedule options, &block
  end
end
