##
# Schedule manages a JobQueue which contains Job instances, and a thread which
# pops and executes them. For simplicity's sake, the Unicron module manages a
# default Schedule, but you can create and manage your own if necessary. Each
# uses its own JobQueue and thread, so they will not interfere with each other.
#
class Unicron::Schedule

  ##
  # Add a Job to the Schedule's JobQueue.
  #
  def << job
    @queue << job
    self
  end

  ##
  # Create a new, empty Schedule. The Schedule's thread is started and blocks
  # immediately, waiting for a Job to be added.
  #
  def initialize
    @queue = Unicron::JobQueue.new
    @thread = Thread.new &method(:run)
  end

  ##
  # Create a new Job and add it to the Schedule. See Job::new for available
  # options.
  #
  def schedule options={}, &block
    job = Unicron::Job.new options, &block
    self << job
    job
  end

  protected

    ##
    # Loops infinitely, popping jobs off the JobQueue and running it. All
    # exceptions are caught and logged to STDERR so that they do not interfere
    # with other jobs. If the Job is repeating, it is added back to the JobQueue
    # after it completes, regardless of whether it raised an exception.
    #
    def run
      while job = @queue.pop
        begin
          job.run
        rescue => err
          puts STDERR, (["#{err.class}: #{err.message}"] +
              err.backtrace.collect {|i| "from #{i}"}).join("\n\t")
        end
        if !job.cancelled &&
            ((job.repeat.is_a?(Numeric) && job.repeat > 0) ||
            (!job.repeat.is_a?(Numeric) && job.repeat))
          self << job
        end
      end
    end
end
