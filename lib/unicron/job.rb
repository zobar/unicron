##
# Job contains all of the information pertinent to a particular task that can be
# managed by Unicron.
#
class Unicron::Job

  ## Time of the first run of this Job.
  attr_reader :at

  ## Set the time of the first run of this Job.
  def at= value
    self.options = {at: value}
  end

  ##
  # True if a cancellation is pending. Only meaningful while the Job is running.
  #
  attr_reader :cancelled

  ## Time before the first run, and between repeats of this Job.
  attr_reader :delay

  ## Set the time before the first run, and between repeats of this Job.
  def delay= value
    self.options = {delay: value}
  end

  ## Time of the next Job run.
  attr_reader :next_run

  ## Time of the previous Job run.
  attr_reader :previous_run

  ## The number of times to repeat, or true for an infinitely repeating Job.
  attr_reader :repeat

  ## Set the number of times to repeat, or true for an infinitely repeating Job.
  def repeat= value
    self.options = {repeat: value}
  end

  ## Proc that gets called when the Job is run.
  attr_reader :task

  ## Set the Proc that gets called when the Job is run.
  def task= value
    self.options = {task: value}
  end

  ## Compare two Job instances chronologically by time of next run.
  def <=> other
    next_run <=> other.next_run
  end

  ## Deschedules the Job.
  def cancel
    if queue
      queue.delete self
    else
      @cancelled = true
    end
  end

  ##
  # Create a new Job. The code to run can be specified either as a block to the
  # constructor or as an option named +task+. If both are specified, the option
  # takes precedence over the block.
  #
  # For non-repeating Jobs, you must use either +at+ or +delay+ to specify when
  # the Job should be run. If +at+ is specified, the job will run at that time
  # (or immediately, if +at+ is in the past). If +delay+ is specified, it will
  # run that number of seconds after it is scheduled.
  #
  # For repeating Jobs, set +repeat+ to the number of times to repeat the Job,
  # or +true+ to repeat infinitely. The +delay+ option becomes mandatory and
  # specifies the number of seconds between invocations. If the +at+ option is
  # specified, it gives the time of the first invocation; otherwise, it first
  # fires +delay+ seconds in the future.
  #
  # For both repeating and non-repeating jobs, if both +at+ and +delay+ are
  # specified, and +at+ is in the past, the first invocation will occur at the
  # first even multiple of +delay+ seconds after +at+. This provides an easy way
  # of ensuring that the jobs are run at the same time every hour or day.
  #
  def initialize options={}, &task
    self.options = options.merge task: task
  end

  ## Return a Hash of this Job's options.
  def options
    {at: at, delay: delay, repeat: repeat, task: task}
  end

  ##
  # Set many Job options at once.
  #
  def options= value
    notify_queue = !queue.nil? && [:at, :delay].any? {|k| value.has_key? k}
    value = {at: at, delay: delay, repeat: repeat, task: task}.merge! value
    if value[:task].nil?
      raise ArgumentError, 'No task specified.'
    end
    if value[:at].nil? && value[:delay].nil?
      raise ArgumentError, 'Either at or delay must be specified.'
    end
    if value[:repeat] && value[:delay].nil?
      raise ArgumentError, 'Repeating jobs must specify delay.'
    end
    @at = value[:at]
    @delay = value[:delay]
    @repeat = value[:repeat]
    @task = value[:task]
    queue.send :jobs_changed if notify_queue
    value
  end

  ##
  # Runs the Job, yielding self.
  #
  def run
    self.repeat -= 1 if repeat.is_a? Numeric
    task.call self
    @previous_run = next_run
  end

  protected
    ## JobQueue that this Job is scheduled on. (Protected)
    attr_reader :queue

    ##
    # Set the JobQueue that this Job is scheduled on.
    #
    def queue= value
      @cancelled = false
      @queue = value
      if queue
        now = Time.now
        base = at || previous_run
        @next_run = if base.nil?
          now + delay
        elsif delay.nil? || base >= now
          base
        else
          base + ((now - base) / delay).ceil * delay
        end
      end
    end
end
