require 'monitor'

##
# JobQueue manages a set of Job instances for a Schedule. Users of Unicron
# should not have to deal directly with JobQueue instances.
#
class Unicron::JobQueue < Monitor

  ## Add a Job to the JobQueue.
  def << job
    synchronize do
      unless @jobs.include? job
        @jobs << job
        job.send :queue=, self
        jobs_changed
      end
      self
    end
  end

  ## Delete a Job from the JobQueue.
  def delete job
    synchronize do
      if @jobs.include? job
        job.send :queue=, nil
        @jobs.delete job
        jobs_changed
      end
      job
    end
  end

  ## Create an empty JobQueue.
  def initialize
    super
    @jobs = []
    @jobs_changed = new_cond
  end

  ##
  # Return the next Job in the JobQueue, sorted chronologically, no earlier than
  # its appointed time. This call will block until the next Job is ready. If the
  # JobQueue is empty, it will block until a Job is added and that Job becomes
  # ready.
  #
  def pop
    synchronize do
      while true
        @jobs_changed.wait_while {@jobs.empty?}
        job = @jobs.first
        delay = job.next_run - Time.now
        break if delay <= 0
        @jobs_changed.wait delay
      end
      job.send :queue=, nil
      @jobs.delete job
    end
  end

  protected

    ## Notify a blocked thread that it should check the queue contents again.
    def jobs_changed
      synchronize do
        @jobs.sort!
        @jobs_changed.signal
      end
    end
end
