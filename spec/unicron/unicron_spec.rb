require 'unicron'

DELAY_IN_SECONDS = 0.5
WIGGLE_ROOM = 0.01
SLEEP_SECONDS = DELAY_IN_SECONDS + WIGGLE_ROOM

describe Unicron do
  context "with a delay" do
    it "should run the block after the interval" do
      counter = 0
      Unicron.schedule delay: DELAY_IN_SECONDS do
        counter += 1
      end
      lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
      # make sure it doesn't repeat
      lambda{ sleep SLEEP_SECONDS }.should_not change{ counter }
    end
    context "with infinite repeats" do
      it "should run the block repeatedly after the interval" do
        counter = 0
        job = Unicron.schedule delay: DELAY_IN_SECONDS, repeat: true do
          counter += 1
        end
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        # good enough..?
        job.cancel
      end
    end
    context "with n repeats" do
      it "should run the block n times after the interval" do
        counter = 0
        job = Unicron.schedule delay: DELAY_IN_SECONDS, repeat: 2 do
          counter += 1
        end
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        lambda{ sleep SLEEP_SECONDS }.should_not change{ counter }
      end
    end
  end
  context "with an absolute time" do
    context "in the past" do
      it "should run the block immmediately" do
        counter = 0
        Unicron.schedule at: (Time.now - 1) do
          counter += 1
        end
        sleep 0.1   # give it a moment..
        counter.should == 1
      end
    end
    context "in the future" do
      it "should run the block at the specified time" do
        counter = 0
        Unicron.schedule at: (Time.now + DELAY_IN_SECONDS) do
          counter += 1
        end
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
      end
    end
    context "with infinite repeats" do
      it "should raise ArgumentError if delay not specified" do
        lambda do Unicron.schedule at: Time.now, repeat: true do end
        end.should raise_error(ArgumentError)
      end
      it "should run the block repeatedly starting on the specified time" do
        counter = 0
        job = Unicron.schedule at: Time.now, repeat: true, delay: DELAY_IN_SECONDS do
          counter += 1
        end
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        # good enough..?
        job.cancel
      end
    end
    context "with n repeats" do
      it "should raise ArgumentError if delay not specified" do
        lambda do Unicron.schedule at: Time.now, repeat: 3 do end
        end.should raise_error(ArgumentError)
      end
      it "should run the block n times starting on the specified time" do
        counter = 0
        job = Unicron.schedule at: Time.now, repeat: 2, delay: DELAY_IN_SECONDS do
          counter += 1
        end
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        lambda{ sleep SLEEP_SECONDS }.should change{ counter }.by(1)
        lambda{ sleep SLEEP_SECONDS }.should_not change{ counter }
      end
    end
  end
end

