Gem::Specification.new do |s|
  s.authors          = ['David P Kleinschmidt', 'Ian C. Anderson']
  s.description      = 'Schedules one-off and repeating background tasks within long-running ruby processes.'
  s.email            = 'david@kleinschmidt.name'
  s.extra_rdoc_files = Dir['**/*.rdoc']
  s.files            = Dir['lib/**/*.rb']
  s.homepage         = 'http://bitbucket.org/zobar/unicron'
  s.name             = 'unicron'
  s.require_paths    = %w[lib]
  s.summary          = 'Schedule background tasks.'
  s.version          = '0.1.2'

  s.add_development_dependency 'rspec', '~> 2.7'
end
